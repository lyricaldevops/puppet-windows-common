# These facts only apply on 64-bit windows

Facter.add("programfilesx86") do

    # assuming linux
    if Facter::Util::Config.is_windows?
      setcode do
        ENV['ProgramFiles(x86)']
      end
    end

end

Facter.add("programfilesw6432") do

    # assuming linux
    if Facter::Util::Config.is_windows?
      setcode do
        ENV['ProgramFilesW6432']
      end
    end

end
