# Using ${cmdexe} instead of the path to cmd.exe makes things more readable
Facter.add("cmdexe") do

    # assuming linux
    if Facter::Util::Config.is_windows?
      setcode do
        ENV['ComSpec']
      end
    end

end
