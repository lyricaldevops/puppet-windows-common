# the appdata path for the user running puppet
Facter.add("appdata") do

    # assuming linux
    if Facter::Util::Config.is_windows?
      setcode do
        ENV['APPDATA']
      end
    end

end
