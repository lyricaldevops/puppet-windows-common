# By default puppet has no knowledge of the Windows systemdrive
Facter.add("systemdrive") do

    # assuming linux
    if Facter::Util::Config.is_windows?
      setcode do
        ENV['SYSTEMDRIVE']
      end
    end

end
