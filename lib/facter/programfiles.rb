# By default puppet has no knowledge of the Windows systemdrive
Facter.add("programfiles") do

    # assuming linux
    if Facter::Util::Config.is_windows?
      setcode do
        ENV['ProgramFiles']
      end
    end

end
