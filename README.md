# Puppet-Windows-Common

While puppet has great support for Microsoft Windows, there are basic additions that make it easier to manage the Windows life cycle.  

License: MIT

## Facts

This module exposes several Microsoft Windows variables that are needed for basic use of Windows:

* systemdrive - The location of the operating system install (i.e. %SYSTEMDRIVE% or C:)
* programfiles - The location of the Program Files folder
* programfilesx86 - The location of the Program Files x86 folder on 64-bit Windows.
* cmdexe - The location of the cmd.exe interpreter
* appdata - The AppData variable for the user running the puppet server

## windows::service

The windows::service definition adds support for service installation and removal, making it trivial to orchestrate Windows service management. As the stock service types for puppet can neither ensure a service is present, or absent, combining it with *windows::service* means that a service lifecycle can be managed. The example below adds the service "my service" and ensures it will both be running, and set to run on reboot.

	windows::service {"my service":
		name => "Lyrical Windows Service",
		shortname => "svcid",
		command => "C:/Windows/DoSomething.exe",
		args => "/something=1 /something_else=2",
		user => "domain\\username",
		password => "s0m3p@ssw0rD",
		ensure => present,
	}
	
	service {"svcid":
	    ensure => running,
	    enable => true,
	    require => Windows::Service["my service"]
	}