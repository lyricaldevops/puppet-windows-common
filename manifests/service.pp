# While puppet has support for windows, the service type can only set a
# Windows service state, and whether or not the service is enabled.
# This module supports installing and removing windows services - and can be
# tied together with the service{} type, in order to manage the Windows service
define windows::service($name, $shortname, $command, $args="", $user, $password, $ensure=present) {

  # windows service control binary
  $scexe = "${::systemdrive}\\Windows\\System32\\sc.exe"

  # puppet won't auto convert these paths since it has no way
  $_command = regsubstr($command, "/", "\\", "G")

  if ($ensure == present) {
    exec {"remove service ${name}":
      command => "{scexe} create ${shortname} binPath= \"${_command} ${args}\" displayname= \"${displayname}\" start = auto obj= \"${user}\" password= \"${password}\"",
      unless => ${scexe} query ${shortname}",
    }
  }

  else {
    exec {"remove service ${name}":
      command => "{scexe} delete ${shortname}",
      onlyif => ${scexe} query ${shortname}",
    }
  }

}
